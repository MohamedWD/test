<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> nomtek - Mobile Application Development Agency</title>
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/style.css" />
        <!--[if lt IE 9]-->
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <!--[endif]-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans" >
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans" >
    </head>
    <body>
        <!-- start header -->
        
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="index.html">Brand</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="whatwedo.html" target="_blank">what we do</a></li>
                        <li><a href="#">company</a></li>
                        <li><a href="#">blog</a></li>
                        <li><a href="#">join us</a></li>
                        <button class="btn btn-danger">contact us</button>
                    </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container -->
            </nav>
        <section class="header">
            <div class="container">
                <div class="intro">
                    <p>go beyond <br>the obvious <span><i class="fa fa-minus"></i></span></p>
                    <div class="traingles">
                        <i class="fa fa-caret-down fa-2x"></i>
                        <i class="fa fa-caret-up fa-2x"></i>
                        <i class="fa fa-caret-down fa-2x"></i>
                        <i class="fa fa-caret-up fa-2x"></i>
                    </div>
                    <h4>we craft apps that change the game</h4>
                    <div class="scroll text-center">
                        <h6>Scroll</h6>
                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
        </section>
        <!-- end header -->
        
        <!-- start slider -->
        <section class="slider">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div  class="carousel-inner" role="listbox">
                    <div id="scene" class="item active">
                        <img src="images/ipad-iphone1.png" alt="Ipad">
                        <div class="carousel-caption">
                            <h3>Heading1</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            <button class="btn btn-default">discover case</button>
                        </div>
                    </div>
                    <div class="item">
                        <img src="images/ipad.png" alt="IPhone">
                        <div class="carousel-caption">
                            <h3>Heading2</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            <button class="btn btn-default">discover case</button>
                        </div>
                    </div>
                    <div class="item">
                        <img src="images/ipadmini-retina.png" alt="IPhone">
                        <div class="carousel-caption">
                            <h3>Heading3</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            <button class="btn btn-default">discover case</button>
                        </div>
                    </div>
                </div> <!-- end of carousel-inner -->

                  <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
        <!-- end slider -->
        
         <!-- start our-clients -->
        <section class="our-clients">
            <div class="container">
                <h1>our clients <sub><i class="fa fa-minus"></i></sub></h1>
                <div class="row text-center">
                    <div class="col-md-3"><i class="fa fa-edge fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-fort-awesome fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-drupal fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-glide-g fa-5x"></i></div>
                    
                </div>
                <div class="row text-center">
                    <div class="col-md-3"><i class="fa fa-css3 fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-skype fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-snapchat fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-vk fa-5x"></i></div>
                    
                </div>
                <div class="row text-center">
                    <div class="col-md-3"><i class="fa fa-youtube fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-windows fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-soundcloud fa-5x"></i></div>
                    <div class="col-md-3"><i class="fa fa-rebel fa-5x"></i></div>
                </div>
            </div>
        </section>
        <!-- end our-clients -->
        
        <!-- start contact -->
        <section class="contact-us">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <ul class="list-unstyled">
                            <li><a href="#">contact@nomtek.com</a></li>
                            <li><a href="#">US +1 888 999-777</a></li>
                            <li><a href="#">UK +44 666 222 7777</a></li>
                            <li><a href="#">PL +48 000 222 111</a></li>
                        </ul> 
                    </div>
                    <div class="col-md-4">
                        <div class="social-links text-center">
                            <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                            <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                            <a href="#"><i class="fa fa-linkedin fa-lg"></i></a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p class="text-right">&copy; 2017 Nomtek. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- end contact -->
        
        <!-- start footer -->
        <a href="#" class=" footer text-center">
            <div class="container">
                <p>want to work with us ? <span><i class="fa fa-long-arrow-right"></i></span></p>
            </div>
        </a>
        <!-- end footer -->
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
    </body>
</html>